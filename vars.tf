variable "env" {
  type        = string
  description = "Environment name: dev, qa, or prd"
  default     = "dev"
}

variable "location" {
  type        = string
  default     = "eastus"
  description = "Azure region."
}

variable "prefix" {
  type        = string
  default     = "compute"
  description = "Prefix for azure object naming convention."
}

variable "ssh_source_address" {
  type        = string
  default     = "*"
  description = "Address(es) to allow ssh access into resource."
}

variable "ssh_pub_aaron_at_code_rodri_us" {
  type        = string
  description = "ssh public key."
}

variable "ARM_SUBSCRIPTION_ID" {
  type        = string
  description = "Azure subscription id."
}

variable "ARM_TENANT_ID" {
  type        = string
  description = "Azure tenant id."
}

variable "ARM_CLIENT_ID" {
  type        = string
  description = "Azure client id."
}

variable "ARM_CLIENT_SECRET" {
  type        = string
  description = "Azure secret."
}
