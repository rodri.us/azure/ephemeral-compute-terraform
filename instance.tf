# ephemeral instance
resource "azurerm_virtual_machine" "vm" {
  name                  = "${var.env}_${var.prefix}_vm"
  location              = var.location
  resource_group_name   = azurerm_resource_group.vm_rg.name
  network_interface_ids = [azurerm_network_interface.vm_nic.id]
  vm_size               = "Standard_B1ms"

  # this is an ephemeral instance, so we can delete all data on termination
  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  plan {
    publisher = "redhat"
    name      = "rhel-lvm85-gen2"
    product   = "rhel-byos"
  }
  storage_image_reference {
    publisher = "redhat"
    offer     = "rhel-byos"
    sku       = "rhel-lvm85-gen2"
    version   = "latest"
  }
  storage_os_disk {
    name              = "${var.env}_${var.prefix}_disk_os"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "${var.env}-${var.prefix}-vm"
    admin_username = "az-user"
    #admin_password = "..."
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      key_data = file("${var.ssh_pub_aaron_at_code_rodri_us}")
      path     = "/home/az-user/.ssh/authorized_keys"
    }
  }
}

resource "azurerm_network_interface" "vm_nic" {
  name                = "${var.env}_${var.prefix}_nic"
  location            = var.location
  resource_group_name = azurerm_resource_group.vm_rg.name

  ip_configuration {
    name                          = "${var.env}_${var.prefix}_nic_ip"
    subnet_id                     = azurerm_subnet.vm_int_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.vm_pub_ip.id
  }
}

resource "azurerm_network_interface_security_group_association" "vm_nic_sga" {
  network_interface_id      = azurerm_network_interface.vm_nic.id
  network_security_group_id = azurerm_network_security_group.vm_sg.id
}

resource "azurerm_public_ip" "vm_pub_ip" {
  name                = "${var.env}_${var.prefix}_pub_ip"
  location            = var.location
  resource_group_name = azurerm_resource_group.vm_rg.name
  allocation_method   = "Dynamic"
}
