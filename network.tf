
resource "azurerm_virtual_network" "vm_network" {
  name                = "${var.env}_${var.prefix}_network"
  location            = var.location
  resource_group_name = azurerm_resource_group.vm_rg.name
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "vm_int_subnet" {
  name                 = "${var.env}_${var.prefix}_int_subnet"
  resource_group_name  = azurerm_resource_group.vm_rg.name
  virtual_network_name = azurerm_virtual_network.vm_network.name
  address_prefixes     = ["10.0.0.0/24"]
}

resource "azurerm_network_security_group" "vm_sg" {
  name                = "${var.env}_${var.prefix}_sg"
  location            = var.location
  resource_group_name = azurerm_resource_group.vm_rg.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = var.ssh_source_address
    destination_address_prefix = "*"
  }
}
