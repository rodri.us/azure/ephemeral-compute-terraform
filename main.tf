terraform {
  backend "http" {
  }
}
provider "azurerm" {
  features {}

  subscription_id = var.ARM_SUBSCRIPTION_ID
  tenant_id       = var.ARM_TENANT_ID
  client_id       = var.ARM_CLIENT_ID
  client_secret   = var.ARM_CLIENT_SECRET
}
# Create a resource group
resource "azurerm_resource_group" "vm_rg" {
  name     = "${var.env}_${var.prefix}"
  location = var.location
}
